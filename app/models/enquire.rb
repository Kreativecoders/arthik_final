# == Schema Information
#
# Table name: enquires
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  service_type :string(255)
#  phno         :string(255)
#  email        :string(255)
#  fromcity     :string(255)
#  tocity       :string(255)
#  shiftdate    :date
#  subject      :string(255)
#  message      :text
#  created_at   :datetime
#  updated_at   :datetime
#

class Enquire < ActiveRecord::Base
end
