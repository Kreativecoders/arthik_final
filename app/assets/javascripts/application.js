// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require "scripts/jquery.min.js" 
//= require "jquery.easing.1.3.js"
//= require "jquery.wookmark.js"
//= require "jquery.yoxview-2.21.js"
//= require "scripts/carousel/jquery.carouFredSel-6.2.0-packed.js" 
//= require "scripts/easing/jquery.easing.1.3.js" 
//= require "scripts/bootstrap/js/bootstrap.min.js" 
//= require "scripts/default.js" 
//= require "scripts/camera/scripts/camera.min.js" 
//= require "scripts/wookmark/js/jquery.wookmark.js" 
//= require "scripts/yoxview/yox.js"
//= require "scripts/yoxview/jquery.yoxview-2.21.js"
//= require "scripts/camera/scripts/camera.min.js"
//= require "scripts/easing/jquery.easing.1.3.js"
//= require "camera.min.js"
//= require "yox.js"
//= require "camera.min.js"

