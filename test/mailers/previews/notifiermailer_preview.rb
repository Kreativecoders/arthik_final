# Preview all emails at http://localhost:3000/rails/mailers/notifiermailer
class NotifiermailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/notifiermailer/notification
  def notification
    Notifiermailer.notification
  end

end
