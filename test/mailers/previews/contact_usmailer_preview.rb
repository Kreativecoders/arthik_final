# Preview all emails at http://localhost:3000/rails/mailers/contact_usmailer
class ContactUsmailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contact_usmailer/welcome
  def welcome
    ContactUsmailer.welcome
  end

end
