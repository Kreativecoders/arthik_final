class CreateEnquires < ActiveRecord::Migration
  def change
    create_table :enquires do |t|
      t.string :name
      t.string :service_type
      t.string :phno
      t.string :email
      t.string :fromcity
      t.string :tocity
      t.date :shiftdate
      t.string :subject
      t.text :message

      t.timestamps
    end
  end
end
